import yaml
from yattag import Doc, indent

# global yattag vars
doc, tag, text, line  = Doc().ttl()

# YAML layout:
    # subsystems: list of subsystem dicts
        # subsystem: dicts with nested dicts or lists as value pairs
            # labels: dict {name, readyForMergeDeps, newLabels, emailLabel}
            # status: value
            # devel-sst: list of values
            # requiredApproval: value
            # maintainers: list of dicts (can have duplicate names but
            # multiple emails)
                # name: string 
                # email: string
                # gluser: string
                # restricted: T/F
                # field is optional, defaults to False
            # paths: dict {includes, includeRegexes}
            # scm: value
            # mailingList: value

# A helper for incoporating css into the doc
def cssStyles():
    with tag('style'):
        doc.asis(
'''
td { vertical-align: top; }
tr.details { display: none; }
tr.details.expanded { display: revert; }
tr.summary > td:first-child::before { content: "⏵ "; }
tr.summary.expanded > td:first-child::before { content: "⏷ "; }
div.devel-sst { margin-bottom: 5px; }
.detail-header { font-weight: bold; }
.detail-columns { width: 100%; column-width: 300px; margin-bottom: 5px; }
''')

# A helper for formatting each subsystem
def fmtSubsystem(subsystem, maintainer):
    paths = subsystem.get('paths', {})
    incpaths = paths.get('includes', None)
    regexpaths = paths.get('includeRegexes', None)
    excpaths = paths.get('excludes', None)

    # Create a table row with a collapsible element
    with tag('tr', ('onclick',
                    'event.currentTarget.classList.toggle("expanded"); '
                    'event.currentTarget.nextElementSibling.classList.toggle("expanded");'),
                   klass='summary'):
        line('td',str(subsystem['subsystem']))
        line('td',str(subsystem['status']))
        line('td',str(maintainer['name']))
        line('td',str(maintainer['email']))

    with tag('tr', klass='details'):
        with tag('td', colspan=4):
            with tag('div', klass='devel-sst'):
                with tag('span', klass='detail-header'):
                    text('devel-sst: ')
                text(', '.join(subsystem["devel-sst"]))
            if incpaths:
                with tag('div', klass='detail-header'):
                    text('Included file paths:')
                with tag('div', klass='detail-columns'):
                    for path in incpaths:
                        text(path)
                        doc.stag('br')
            if excpaths:
                with tag('div', klass='detail-header'):
                    text('Excluded file paths:')
                with tag('div', klass='detail-columns'):
                    for path in excpaths:
                        text(path)
                        doc.stag('br')
            if regexpaths:
                with tag('div', klass='detail-header'):
                    text('Included paths matching regex:')
                with tag('div', klass='detail-columns'):
                    for path in regexpaths:
                        text(path)
                        doc.stag('br')

# Passing the dict data to this function
# Here, we iterate over every item in the list, and programatically make
# a static HTML file which demonstrates this
def makeHTML(data):
    doc.asis(
'''+++
+++
''')
    with tag('html'):
        with tag('head'):
            cssStyles()
            with tag('title'):
                text('Maintainers List')

        with tag('body', id = 'maintinfo'):
            with tag('text'):
                line('p', 'Generated from owners.yaml')

            with tag('p'):
                line('p','For questions/concerns/comments, message #team-kernel-maint on Slack.')
                line('p','Or, send an email to the kernel maintainers SST.')

            with tag('p'):
                with tag('a', href ='//gitlab.com/redhat/centos-stream/src/kernel/documentation'
                        ):
                    text('Alternatively, submit an MR to gitlab.com/redhat/centos-stream/src/kernel/documentation')

            with tag('table'):
                with tag('tr'):
                    line('th','Subsystem')
                    line('th','Status')
                    line('th','Maintainer')
                    line('th','E-Mail')

                for item in data['subsystems']:
                    for element in item['maintainers']:
                        fmtSubsystem(item, element)

    return(indent(doc.getvalue()))

# unmarshall the raw decoded YAML data into a dict object. 
def deserializeYAML():
    with open('../../info/owners.yaml') as f:
        # Load the YAML as a dictionary of values.
        return yaml.safe_load(f)

# This file will be the main entry point for the generator
def main():
    yaml = deserializeYAML()
    html = makeHTML(yaml)

    file = open('../../content/docs/maintainers-list.html', 'w')
    file.write(html)
    file.close

if __name__ == "__main__":
    main()

