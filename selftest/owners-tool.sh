#!/bin/bash
cd owners-tool
errors=0
count=0
for f in *.yaml *.noyaml; do
	name=${f%.*}
	output=$(python3 ../../scripts/owners-tool.py verify "$f" ../../templates/owners-schema.yaml 2>&1)
	if [[ -f "$name.result" ]]; then
		expected=$(< "$name.result")
	else
		expected=
	fi
	if [[ "$output" != "$expected" ]]; then
		echo "FAILED: $name"
		echo "------ output ------"
		echo "$output"
		echo "----- expected -----"
		echo "$expected"
		echo "--------------------"
		(( errors++ ))
	fi
	(( count++ ))
done
if (( errors == 0 )); then
	echo "OK $count tests"
else
	echo "FAILED $errors of $count tests"
fi
(( errors == 0 ))
