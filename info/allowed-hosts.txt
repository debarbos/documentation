#
# This file includes a list of Red Hat internal hostnames and regex pattern
# matches approved by Red Hat Information Security to be exposed publicly.
# It may be consumed, for example, by CI pipelines that wish to ensure that any
# *redhat.com links added to a repository matches any of the listed matches
# from below, otherwise said content must be rejected.
#
# All additions to this file must be accompanied by a ticket number containing
# Risk Management's approval to disclose said URL. Most entries below are
# permitted via INC2520085 unless a comment indicates otherwise.
#

# Beaker Machines
[a-zA-Z0-9\\.\\-]+pek2.redhat.com
[a-zA-Z0-9\\.\\-]+.eng.[a-zA-Z0-9\\-]+(.dc)?.redhat.com
rhts.redhat.com

# Internal hosts
brewweb.engineering.redhat.com
brew-task-repos.usersys.redhat.com
clock.redhat.com
cts.engineering.redhat.com # Based on INC3431775
download.devel.redhat.com
download[a-zA-Z0-9\\.\\-]*.eng.[a-zA-Z0-9\\-]+.redhat.com
download.lab.bos.redhat.com
vmcore.usersys.redhat.com
rhivos.auto-toolchain.redhat.com # Based on INC2968163

# Public hosts - no explicit approval required
access.redhat.com
bugzilla.redhat.com
issues.redhat.com
sso.redhat.com
umb.api.redhat.com # Based on INC3431775
www.redhat.com
